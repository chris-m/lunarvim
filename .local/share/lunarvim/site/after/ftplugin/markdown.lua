require("lvim.lsp.manager").setup("grammarly")
require("lvim.lsp.manager").setup("zk")
require("lvim.lsp.manager").setup("ltex")
require("lvim.lsp.manager").setup("tailwindcss")
